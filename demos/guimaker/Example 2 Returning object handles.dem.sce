mode(1)
// Another example from http://wiki.scilab.org/howto/guicontrol
pg=list();
pg($+1)=list(list('text','exposure time','Horizontalalignment','center'));
pg($+1)=list(list('slider','','Min',0,'Max',100,'Value',5));
pg($+1)=list(list('text','arevscale','Horizontalalignment','center'));
pg($+1)=list(list('slider','','Min',0,'Max',100,'Value',50));
pg($+1)=list(list([1 2],'pushbutton','STOP','callback','OK=%t'),list(2,'radiobutton','bin x2'));
pg($+1)=list(list(1),list(2,'radiobutton','free/trig'));
 
// Create gui and return a list of handles to objects in the gui:
h=guimaker(pg,list('objfigure1',220),[],2)  // returns a vector of object handles
// The object handles may be used in an external event control loop.
