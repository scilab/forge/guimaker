mode(-1)
// Demo of axes and the use of callback.
// Compare this example with the original coding based on uicontrol objects in
// SCI\modules\gui\demos\cb_uicontrol_plot3d.dem.sci
pg=list(); wp=3;
pg($+1)=list(list([1 17],'frame','Control Panel'),list([wp 15],'axes','title.text','My Beautiful Plot'));
pg($+1)=list(list([1 2 1],'frame','Rotation angle:'),list(wp));
pg($+1)=list(list([1 1 2],'slider','','Max',360,'value',80,'callback','ctr3d(''''PlotAngle'''')'),list(wp));
pg($+1)=list(list([1 4 1],'frame','Colormap'),list(wp));
pg($+1)=list(list([1 1 2],'radiobutton','Jetcolormap','value',1,'tag','jet','callback','ctr3d(''''ColorMap'''')'),list(wp));
pg($+1)=list(list([1 1 2],'radiobutton','Hotcolormap','value',0,'tag','hot','callback','ctr3d(''''ColorMap'''')'),list(wp));
pg($+1)=list(list([1 1 2],'radiobutton','Graycolormap','value',0,'tag','gray','callback','ctr3d(''''ColorMap'''')'),list(wp));
pg($+1)=list(list([1 2 1],'frame','Background Color'),list(wp));
pg($+1)=list(list(1/3),list(1/3,'pushbutton','','callback','ctr3d(''''BackColor'''')'),list(1/3),list(wp));
pg($+1)=list(list([1 5 1],'frame','Show/Hide'),list(wp));
pg($+1)=list(list([1 1 2],'checkbox','Tics','value',1,'callback','ctr3d(''''SHTics'''')'),list(wp));
pg($+1)=list(list([1 1 2],'checkbox','Title','value',1,'callback','ctr3d(''''SHTitle'''')'),list(wp));
pg($+1)=list(list([1 1 2],'checkbox','Labels','value',1,'callback','ctr3d(''''SHLabels'''')'),list(wp));
pg($+1)=list(list([1 1 2],'checkbox','Edges','value',1,'callback','ctr3d(''''SHEdges'''')'),list(wp));
pg($+1)=list(list([1 2 1],'frame','Title'),list(wp));
pg($+1)=list(list([1 1 2],'edit','My Beautiful Plot','callback','ctr3d(''''Title'''')'),list(wp));
h=guimaker(pg,list('Control Plot3d',850,[],[],[1 1 1]),list('backgroundcolor',[1 1 1]),2);   // Create the gui using guimaker.
demo_viewCode("cb_guimaker_plot3d.dem.sce");
// 
//  The rest is related to plots and the event function. 
//
f = gcf();	f.background = -2; f.color_map = jetcolormap(128);
x = %pi * [-1:0.05:1]';z = sin(x)*cos(x)'; [x,y,z] = genfac3d(x,x,z); // generate facet coordinates
facetsColors=(z+1)*(128-1)/2+1; // generate colors depending on z, between 1 and colormap size
plot3d(x,y,list(z, facetsColors)); // plot
h=gce(); h.color_flag=3; ax=gca();	ax.rotation_angles(1) = 80; // rotate figure
// Create the event control function.
function ctr3d(cmd)
  // Event control function called by gui object callback functions.
  ax=gca();
  select cmd
    case 'PlotAngle'  // Rotate figure
      angle = get(gcbo,"Value");
      ax.rotation_angles(2) = angle;
    case 'ColorMap'   // Select colormap
      set(findobj('tag','jet'),'value',0);
      set(findobj('tag','hot'),'value',0);
      set(findobj('tag','gray'),'value',0);
      set(gcbo,'value',1);
      map=get(gcbo,'tag');
      fig = gcf();	fig.immediate_drawing = "off";
      my_cur_color_map = fig.color_map; 
      my_cur_bg_color_id  = ax.background;
      if my_cur_bg_color_id > 32 then
        my_cur_bg_color_r = my_cur_color_map(my_cur_bg_color_id,1);
        my_cur_bg_color_g = my_cur_color_map(my_cur_bg_color_id,2);
        my_cur_bg_color_b = my_cur_color_map(my_cur_bg_color_id,3);
      end
      my_wanted_colormap = get(gcbo,"tag");
      if map == "jet" then
        fig.color_map = jetcolormap(128);
      elseif map == "hot" then
        fig.color_map = hotcolormap(128);
      elseif map == "gray" then
        fig.color_map = graycolormap(128);
      end
      if my_cur_bg_color_id > 32 then	
        color_id = color(my_cur_bg_color_r*255,my_cur_bg_color_g*255,my_cur_bg_color_b*255);
        ax.background = color_id;
      end
      fig.immediate_drawing = "on";
    case 'BackColor'  //  Change background color
      [r,g,b]=uigetcolor();
      if (~isempty(r) & ~isempty(g) & ~isempty(b)) then
        set(gcbo,'backgroundcolor',[r g b]/255);
        ax.background=color(r,g,b);
      end
    case 'SHTics'   // Show/hide Tics
      if get(gcbo,'value') then        
        ax.axes_visible=["on" "on" "on"];
      else
        ax.axes_visible=["off" "off" "off"];
      end    
    case 'SHTitle'  // Show/hide Title
      if get(gcbo,'Value') then
        ax.title.visible="on";
      else
        ax.title.visible="off";
      end
    case 'SHLabels' // Show/hide Labels
      if get(gcbo,'Value') then
        ax.x_label.visible="on";
        ax.y_label.visible="on";
        ax.z_label.visible="on";
      else
        ax.x_label.visible="off";
        ax.y_label.visible="off";
        ax.z_label.visible="off";
      end
    case 'SHEdges'  // Show/hide Edges
      plt=ax.children;
      if get(gcbo,'value') then
        plt.color_mode=+1;
      else
        plt.color_mode=-1;
      end      
    case 'Title'   // Edit title
      ax.title.text=get(gcbo,'string');
  end
endfunction
